<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    /**
     * Nombre de la tabla
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title', 'resume', 'body',
        'category_id', 'author_id', 'imagen'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * Permite obtener la categoria de un post
     *
     * @return void
     */
    public function getCategories()
    {
        return $this->belongsTo(Categories::class, 'category_id', 'id');
    }

    /**
     * Permite obtener el nombre de la categoria
     *
     * @return string
     */
    public function getNameCategory():string
    {
        return $this->getCategories->name;
    }

    /**
     * Permite al autor de un post
     *
     * @return void
     */
    public function getAuthors()
    {
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    /**
     * Permite obtener el nombre del autor
     *
     * @return string
     */
    public function getNameAuthor():string
    {
        return $this->getAuthors->name;
    }
}

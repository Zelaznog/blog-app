<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;

    /**
     * Nombre de la tabla
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Permite obtener los posts pertenecientes a una categoria en especifico
     *
     * @return void
     */
    public function getPosts()
    {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }
}

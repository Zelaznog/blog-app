<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    /**
     * Nombre de la tabla
     *
     * @var string
     */
    protected $table = 'authors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Permite obtener los posts pertenecientes a un autor en especifico
     *
     * @return void
     */
    public function getPosts()
    {
        return $this->hasMany(Post::class, 'author_id', 'id');
    }
}

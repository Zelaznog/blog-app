<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use App\Http\Controllers\MetricaController;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Post');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Permite obtener todos los posts agregados
     *
     * @return string
     */
    public function allPost(): string
    {
        try {
            $post = Post::with(['getAuthors', 'getCategories'])->orderBy('id', 'desc')->paginate(5)->toJson();
            return $post;
        } catch (\Throwable $th) {
            Log::error('PostController -> allPost -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'title' => ['required'],
            'resume' => ['required'],
            'body' => ['required'],
            'category_id' => ['required', 'numeric'],
            'author_id' => ['required', 'numeric'],
            'img' => ['nullable', 'mimes:jpeg,jpe,png']
        ]);

        DB::beginTransaction();
        try {
            if ($validate) {    
                $post = Post::create($request->all());
                $path = '';
                if ($request->file('img')) {
                    $file = $request->file('img');
                    $name = uniqid().'.'.$file->getClientOriginalExtension();
                    $path = $request->file('img')->storeAs(
                        'posts', $name
                    );
                    $post->imagen = $path;
                    $post->save();
                }
                DB::commit();
                return Post::with(['getAuthors', 'getCategories'])->orderBy('id', 'desc')->paginate(5)->toJson();
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('PostController -> store -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $metricaController = new MetricaController();
            $metricaController->saveMetrica($id);

            $post = Post::find($id);
            $post->author = $post->getNameAuthor();
            $post->category = $post->getNameCategory();

            $postAnterior = Post::find($id-1);
            $postSiguiente = Post::find($id+1);

            $post->previous = null;
            $post->next = null;
            if (!empty($postAnterior)) {
                $post->previous = ($id-1);
            }

            if (!empty($postSiguiente)) {
                $post->next = ($id+1);
            }
            return $post->toJson();
        } catch (\Throwable $th) {
            Log::error('PostController -> show -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validate = $request->validate([
            'title' => ['required'],
            'resume' => ['required'],
            'body' => ['required'],
            'category_id' => ['required', 'numeric'],
            'author_id' => ['required', 'numeric'],
            'img' => ['nullable', 'mimes:jpeg,jpe,png']
        ]);

        DB::beginTransaction();
        try {
            if ($validate) {    
                $post = Post::find($id);
                $post->title = $request->title;
                $post->resume = $request->resume;
                $post->body = $request->body;
                $post->category_id = $request->category_id;
                $post->author_id = $request->author_id;
                
                $path = '';
                if ($request->file('img')) {
                    $file = $request->file('img');
                    $name = uniqid().'.'.$file->getClientOriginalExtension();
                    $path = $request->file('img')->storeAs(
                        'posts', $name
                    );
                    $post->imagen = $path;
                }
                $post->save();
                DB::commit();
                return Post::with(['getAuthors', 'getCategories'])->orderBy('id', 'desc')->paginate(5)->toJson();
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('PostController -> update -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            Post::where('id', $id)->delete();
            $post = Post::with(['getAuthors', 'getCategories'])->orderBy('id', 'desc')->paginate(5)->toJson();
            DB::commit();
            return $post;
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('PostController -> destroy -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }
}

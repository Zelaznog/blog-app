<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Inertia::render('Authors');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Permite obtener todos los Autores para listar y modificar
     *
     * @return string
     */
    public function allAuthors(): string
    {
        try {
            $author = Author::orderBy('id', 'desc')->paginate(5)->toJson();
            return $author;
        } catch (\Throwable $th) {
            Log::error('AuthController -> allAuthors -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Permite obtener todos los Autores para listar en un select
     *
     * @return string
     */
    public function allAuthorsList(): string
    {
        try {
            return Author::select('id', 'name')->get()->toJson();
        } catch (\Throwable $th) {
            Log::error('AuthController -> allAuthorsList -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => ['required'],
        ]);

        DB::beginTransaction();
        try {
            if ($validate) {    
                Author::create($request->all());
                DB::commit();
                return Author::orderBy('id', 'desc')->paginate(5)->toJson();
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('AuthorController -> store -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'name' => ['required'],
        ]);

        DB::beginTransaction();
        try {
            if ($validate) {    
                $data = [
                    'name' => $request->name,
                ];
                Author::where('id', $id)->update($data);
                DB::commit();
                return Author::orderBy('id', 'desc')->paginate(5)->toJson();
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('AuthorController -> update -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            Author::where('id', $id)->delete();
            $author = Author::orderBy('id', 'desc')->paginate(5)->toJson();
            DB::commit();
            return $author;
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('AuthorController -> destroy -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }
}

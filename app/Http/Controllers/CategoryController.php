<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Inertia::render('Category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Permite obtener todos las categorias para listar y modificar
     *
     * @return string
     */
    public function allCategory(): string
    {
        try {
            $categories = Categories::orderBy('id', 'desc')->paginate(5)->toJson();
            return $categories;
        } catch (\Throwable $th) {
            Log::error('CategoryController -> allCategory -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Permite obtener todos las categorias para listar en un select
     *
     * @return string
     */
    public function allCategoryList(): string
    {
        try {
            return Categories::select('id', 'name')->get()->toJson();
        } catch (\Throwable $th) {
            Log::error('CategoryController -> allCategoryList -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => ['required'],
        ]);

        DB::beginTransaction();
        try {
            if ($validate) {    
                Categories::create($request->all());
                DB::commit();
                return Categories::orderBy('id', 'desc')->paginate(5)->toJson();
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('CategoryController -> store -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validate = $request->validate([
            'name' => ['required'],
        ]);

        DB::beginTransaction();
        try {
            if ($validate) {    
                $data = [
                    'name' => $request->name,
                ];
                Categories::where('id', $id)->update($data);
                DB::commit();
                return Categories::orderBy('id', 'desc')->paginate(5)->toJson();
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('CategoryController -> update -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try {
            Categories::where('id', $id)->delete();
            $author = Categories::orderBy('id', 'desc')->paginate(5)->toJson();
            DB::commit();
            return $author;
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('CategoryController -> destroy -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Metrica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use Jenssegers\Agent\Facades\Agent;

class MetricaController extends Controller
{
    //
    /**
     * Lleva a la vista de metrica
     *
     * @return void
     */
    public function index()
    {
        return Inertia::render('Metrica');
    }

    /**
     * Permite guardar las metricas
     *
     * @param integer $idpost
     * @return void
     */
    public function saveMetrica(int $idpost)
    {
        DB::beginTransaction();
        try {
            $agent = new Agent();
            $data = [
                'post_id' => $idpost,
                'ipuser' => request()->ip(),
                'plataform' => $agent::platform(),
                'browser' => $agent::browser(),
                'is_divice' => $agent::isMobile()
            ];

            $checkMetrica = Metrica::where($data)->first();
            if (is_null($checkMetrica)) {
                Metrica::create($data);
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('MetricaController -> saveMetrica -> Error: '.$th);
            abort(500, 'Ocurrio un error, por favor contacte con el administrador');
        }
    }

    /**
     * Permite obtener las publicaciones mas vistas
     *
     * @return string
     */
    public function getPublicaciones(): string
    {
        $publicaciones = Metrica::selectRaw('count(id) as total, post_id as post')->orderBy('total', 'desc')->groupBy('post_id')->take(5)->get();
        $posts = [];
        $total = [];
        foreach ($publicaciones as $post) {
            $posts[] = $post->post;
            $total[] = $post->total;
        }

        $data = [
            'post' => $posts,
            'total' => $total
        ];
        return json_encode($data);
    }

     /**
     * Permite obtener los dispositivos mas usuados
     *
     * @return string
     */
    public function getDivices(): string
    {
        $publicaciones = Metrica::selectRaw("COUNT(id) as total, IF (is_divice = 1, 'Mobile', 'Desktop') as divice")->groupBy('is_divice')->get();
        $divices = [];
        $total = [];
        foreach ($publicaciones as $post) {
            $divices[] = $post->divice;
            $total[] = $post->total;
        }

        $data = [
            'divices' => $divices,
            'total' => $total
        ];
        return json_encode($data);
    }

     /**
     * Permite obtener las plataforma mas usuados
     *
     * @return string
     */
    public function getPlataforma(): string
    {
        $publicaciones = Metrica::selectRaw("COUNT(id) as total, plataform")->groupBy('plataform')->get();
        $plataform = [];
        $total = [];
        foreach ($publicaciones as $post) {
            $plataform[] = $post->plataform;
            $total[] = $post->total;
        }

        $data = [
            'plataform' => $plataform,
            'total' => $total
        ];
        return json_encode($data);
    }

     /**
     * Permite obtener las plataforma mas usuados
     *
     * @return string
     */
    public function getNavegador(): string
    {
        $publicaciones = Metrica::selectRaw("COUNT(id) as total, browser")->groupBy('browser')->get();
        $browsers = [];
        $total = [];
        foreach ($publicaciones as $post) {
            $browsers[] = $post->browser;
            $total[] = $post->total;
        }

        $data = [
            'browsers' => $browsers,
            'total' => $total
        ];
        return json_encode($data);
    }


}

-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-01-2022 a las 05:42:00
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blogapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authors`
--

CREATE TABLE `authors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `authors`
--

INSERT INTO `authors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Krystal Greenholt', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(2, 'Lauretta Hand', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(3, 'Scottie Jerde', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(4, 'Joey Balistreri', '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(5, 'Earl Lakin', '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(6, 'John Doe', '2022-01-31 07:32:46', '2022-01-31 07:32:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'occaecati', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(2, 'est', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(3, 'ut', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(4, 'repellendus', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(5, 'et', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(6, 'aut', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(7, 'aliquam', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(8, 'omnis', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(9, 'corrupti', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(10, 'facilis', '2022-01-31 07:07:28', '2022-01-31 07:07:28'),
(11, 'Informatica', '2022-01-31 07:32:29', '2022-01-31 07:32:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metricas`
--

CREATE TABLE `metricas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `ipuser` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'direccion ip del usuario',
  `plataform` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'plataforma donde visualizo el post',
  `browser` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'navedador donde se visualizo el post',
  `is_divice` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 - desktop, 1 - mobile',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `metricas`
--

INSERT INTO `metricas` (`id`, `post_id`, `ipuser`, `plataform`, `browser`, `is_divice`, `created_at`, `updated_at`) VALUES
(1, 14, '127.0.0.1', 'Windows', 'Firefox', 1, '2022-01-31 07:26:36', '2022-01-31 07:26:36'),
(2, 10, '127.0.0.1', 'Windows', 'Edge', 0, '2022-01-31 07:33:09', '2022-01-31 07:33:09'),
(3, 15, '127.0.0.1', 'Windows', 'Chrome', 0, '2022-01-31 07:33:17', '2022-01-31 07:33:17'),
(4, 16, '127.0.0.1', 'Windows', 'Chrome', 1, '2022-01-31 07:33:21', '2022-01-31 07:33:21'),
(5, 16, '127.0.0.1', 'Windows', 'Firefox', 0, '2022-01-31 08:02:07', '2022-01-31 08:02:07'),
(6, 16, '127.0.0.1', 'Windows', 'Chrome', 0, '2022-01-31 08:22:26', '2022-01-31 08:22:26'),
(7, 15, '127.0.0.1', 'Windows', 'Firefox', 0, '2022-01-31 08:24:27', '2022-01-31 08:24:27'),
(8, 14, '127.0.0.1', 'Windows', 'Firefox', 0, '2022-01-31 08:25:16', '2022-01-31 08:25:16'),
(9, 13, '127.0.0.1', 'Windows', 'Firefox', 0, '2022-01-31 08:25:20', '2022-01-31 08:25:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(49, '2014_10_12_000000_create_users_table', 1),
(50, '2014_10_12_100000_create_password_resets_table', 1),
(51, '2019_08_19_000000_create_failed_jobs_table', 1),
(52, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(53, '2022_01_27_034815_create_categories_table', 1),
(54, '2022_01_27_035751_create_authors_table', 1),
(55, '2022_01_27_035936_create_posts_table', 1),
(56, '2022_01_30_044110_create_metricas_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'texto completo',
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `title`, `resume`, `imagen`, `body`, `category_id`, `author_id`, `created_at`, `updated_at`) VALUES
(1, 'Architecto harum nesciunt.', 'Velit quibusdam odit libero consequuntur rerum. Animi id sit itaque unde voluptatem sit eum. Dolores inventore nam ut dicta non et.', NULL, 'Et vel cupiditate laborum veniam ut consectetur. Repellendus quaerat est ex quo nihil nisi omnis praesentium. Ea voluptas hic eum iste. Quia eveniet officia laborum dolorum et et officiis.\n\nReprehenderit tenetur deserunt quo praesentium enim et facilis. Voluptates iste aut nihil adipisci voluptatibus ipsam id. Repellat exercitationem molestiae vel soluta soluta totam quo. A eos corrupti nam.\n\nMagnam aspernatur veniam optio quam eos dicta. Ea ut earum asperiores aut id incidunt. Atque omnis quia impedit totam.\n\nAut et quidem et nihil eveniet delectus rerum laudantium. Qui nihil rerum qui. Eveniet ea vel pariatur.\n\nDoloribus repudiandae blanditiis aliquam officia minima. Ea impedit natus tempore odio doloribus. Non dolore ut quae eum nesciunt est ut. Adipisci libero et consequatur similique quibusdam sequi excepturi.\n\nMaxime amet ratione odio maiores sit doloribus eos. Quia est consequatur sed et ea. Tempore distinctio amet at facilis ut quis autem.\n\nAut ad architecto labore in voluptate ex fuga. Consequatur sapiente quod fugiat perferendis quam nesciunt deleniti ex. Nobis numquam earum in fugiat.\n\nDeleniti animi praesentium est voluptatem itaque. Autem non quas omnis voluptatem et.\n\nNostrum repudiandae consequatur dolore nesciunt. A praesentium voluptatem voluptatem culpa aspernatur mollitia. Aut inventore aut perspiciatis ut velit ad voluptatibus ratione. Qui hic itaque ipsam reiciendis similique et. Sint voluptatem distinctio quisquam vel dolor.\n\nRepellat voluptas incidunt adipisci ipsum. Perspiciatis nulla tempore exercitationem et nam voluptatem maxime minima. Rerum architecto sequi est rerum animi. Facere excepturi voluptates incidunt est.', 6, 2, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(2, 'Illo amet et soluta.', 'Et soluta inventore recusandae fuga ab impedit. Cupiditate et nemo fugit voluptatem. Accusamus iusto voluptatem maiores ullam.', NULL, 'Laboriosam error qui maxime esse consequatur. Nam maxime architecto sit veniam quidem. Dolorum provident temporibus illo quos et.\n\nModi molestiae eius minus aliquid. Quasi distinctio sit consequatur omnis. Cum eligendi corporis et omnis ad.\n\nIpsa deserunt id omnis. Aut ad ut ad adipisci consequatur et. Qui et totam corporis quia est animi. Et soluta iure consequatur non.\n\nAut tempore modi voluptas quo sit. Eveniet quia qui consectetur. Vitae ipsa sint a velit magnam est voluptatibus.\n\nBeatae quam eveniet neque non id et. Commodi autem saepe officiis qui neque. Quia ad vel sed accusantium eos minus quo. Nobis provident pariatur aut sint sit cupiditate odit. Velit minus ex doloremque voluptate ea ut aspernatur.\n\nNobis veniam vitae amet consequatur aut non omnis. Qui dolores eos nisi fuga quasi corporis. Et magni rerum architecto dolores aperiam quis vero.\n\nDignissimos cum reprehenderit cumque atque voluptatem ullam. Eveniet asperiores numquam et commodi totam atque. Culpa eveniet ut quia aut occaecati optio dolores esse.\n\nUt cumque similique corporis dolorem itaque vel nam. Sit in rerum quia aliquid aut accusamus provident consequatur. Consequatur perferendis rerum nam et nostrum facilis deserunt. Nihil omnis ut consequuntur reprehenderit quis nobis distinctio.\n\nQuia ut molestiae incidunt distinctio aut non. Aliquid dolorem veniam cumque. Maiores et nam consequatur dolores. Iure corporis impedit et vero quia.\n\nNatus possimus eligendi amet pariatur et dignissimos. Non aut asperiores eum architecto. Ipsa nobis delectus necessitatibus fugit quis. Aspernatur sunt dolorum velit sint expedita. Aliquam dolor aut doloribus delectus iure ut.', 6, 1, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(3, 'Voluptatem ut atque.', 'Omnis aut cum possimus repudiandae soluta omnis. Et dolor saepe quos dicta beatae. Quam autem iure velit quia sunt eos iure aperiam. Quasi atque vel quibusdam quibusdam at delectus.', NULL, 'Ullam porro quas perspiciatis et numquam ducimus. Dolor porro veritatis fugit officiis et. Vel praesentium occaecati corporis praesentium quo nostrum.\n\nMaiores iste ad ut aspernatur est. Non et rerum ab ad esse ea expedita. Eos omnis sequi culpa et quo illum nemo eius.\n\nRerum aut aut deserunt itaque reprehenderit esse. Omnis maiores et quo tenetur. Facere fugit blanditiis est eaque sunt itaque.\n\nMagni laudantium neque eum nihil placeat. Quis eaque at rerum ullam porro atque. Maxime velit a et dolorem. Perspiciatis voluptatem harum quaerat blanditiis molestiae repudiandae voluptate aliquid. Et aut molestiae eos quaerat earum dicta quis laudantium.\n\nMolestias iusto eligendi eos occaecati. Numquam voluptatem quibusdam dolores qui incidunt ratione fugit. Cupiditate reprehenderit quia non saepe fugiat cupiditate.\n\nInventore reprehenderit reprehenderit ipsa. Id harum et itaque sed. Fuga iure in et assumenda rerum.\n\nA ipsam earum accusantium dolores ex et. Ipsam reiciendis dolores enim libero illum. Quis quibusdam autem facilis quos et et.\n\nVel et ut omnis nihil voluptatem velit illo. Ad doloribus veritatis quia velit cupiditate. Nesciunt nesciunt praesentium quam et.\n\nBlanditiis reprehenderit illum in consequatur exercitationem. Sunt est consequatur quasi repellendus maxime blanditiis voluptatem a. Repellendus qui nisi quia est modi enim maiores. Repellendus eum rerum odit dignissimos occaecati et.\n\nTemporibus id qui voluptatem ut molestiae aut. Et qui minima praesentium repudiandae. Ut dicta ipsa modi officia id consequatur. Quisquam voluptatem praesentium rerum id modi error amet accusamus.', 10, 2, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(4, 'Ea non quia.', 'Officiis quod et nobis illo minus. Voluptas quas a fuga rerum dolore aspernatur aliquam praesentium. Sit in sit possimus voluptates aut illum.', NULL, 'Cum asperiores rerum omnis omnis sint. Eius assumenda suscipit aliquid eum et. Dolor qui id pariatur molestias. Id non dolorem libero rerum ut optio. Quisquam dicta animi debitis.\n\nMollitia alias quo occaecati impedit modi placeat rerum voluptatem. Corporis eligendi modi aut quia a veniam eum rerum. Sit nam quia illo esse.\n\nQuasi ut eum quod dolorem reprehenderit rerum consequatur. Voluptatum ea repellendus tempore sunt voluptatibus. Modi qui atque modi nemo assumenda. Voluptatibus accusamus dolorum commodi recusandae et.\n\nEa enim sit nulla deserunt quibusdam nisi at. Doloremque occaecati dolores quia nihil quo quia. Et impedit similique dolore repudiandae incidunt rerum commodi perferendis. Similique reprehenderit accusantium voluptas nisi et temporibus est.\n\nQuis similique et qui aliquam ratione. Expedita velit itaque sed eos exercitationem nam. Doloremque quisquam perferendis culpa similique est. Molestias in perferendis non beatae accusantium provident.\n\nDeserunt maiores commodi velit dicta rerum. Sit quibusdam quis ut dolorem praesentium. Quo voluptatem rerum illo minus aut saepe aut.\n\nCorrupti libero sequi aliquam ut doloribus laborum. Reprehenderit consectetur ipsam qui est veniam qui voluptatibus qui. Sunt libero tenetur distinctio ex iste.\n\nLaboriosam est quibusdam dolor voluptas fugiat officiis suscipit ut. Tempore autem dolor quia assumenda veniam. Error quia facilis libero in aut. Et aut rerum ea sapiente suscipit.\n\nEarum qui aut aspernatur ratione nobis quam officia. Sit id laborum blanditiis reprehenderit. Animi et labore voluptates reiciendis corrupti culpa.\n\nNecessitatibus deserunt facilis voluptates praesentium. Amet velit quisquam repellendus cum sint dolor. Officiis quia optio ut eaque temporibus quod sunt.', 9, 2, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(5, 'Quia quia sunt non.', 'Neque dolorem qui nulla non iste nulla. Enim in modi iste sint. Qui animi qui quo.', NULL, 'Sapiente voluptatem harum optio provident aut et vel. A est suscipit quia quia incidunt voluptates. Cumque incidunt distinctio dolor. Incidunt distinctio est molestiae.\n\nEa delectus hic rerum. Molestiae temporibus dolor et sunt id. Vero perspiciatis aut esse perspiciatis omnis non ut. Delectus ad dolorem accusantium culpa ut sit.\n\nEst ipsum non aut. Et praesentium est doloribus unde. Placeat doloribus placeat earum natus.\n\nIllo sapiente accusamus dolor qui non. Dolorem et eos nesciunt animi eum. Harum iusto quidem et perferendis.\n\nVoluptatem nesciunt ipsum libero. Quas accusantium nostrum quasi et exercitationem. Illo in laudantium eum cupiditate voluptas dolor beatae.\n\nDeleniti aliquam atque saepe harum id error dolor laborum. Architecto qui voluptatibus harum amet non doloribus. Minus ratione unde quia.\n\nVoluptatem rerum illum ipsum. Iste pariatur aut sunt perspiciatis numquam qui. Quia dignissimos corrupti consequatur atque asperiores. Sequi ipsam consectetur laudantium quis.\n\nMolestiae explicabo adipisci voluptatem nulla tempore corrupti quis. Ut placeat rem nihil provident iusto et qui. Et ab sint reprehenderit. Cumque perferendis quod molestiae ex dolorem repudiandae.\n\nNesciunt ipsum consectetur temporibus rem non. Numquam excepturi inventore totam ut repellendus. In quibusdam recusandae doloribus qui. Et aut officia nulla dolorum possimus. Libero aut alias tenetur corrupti quia.\n\nQuae explicabo dolores voluptatem. Repellat et quo et tempore culpa labore ut. Debitis deleniti quia sequi quam exercitationem. Delectus quas atque fugiat voluptas.', 9, 1, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(6, 'Totam iure neque.', 'Quo voluptatem veniam non quia id quos. Quo et id explicabo. Quia perferendis consequuntur aut nulla. Modi quaerat aut velit maxime eos et et ut.', NULL, 'Ea ut modi molestiae repellat laborum dolor libero. Vitae voluptatem ratione quo dolorem ullam. Aut perspiciatis id porro. Est quae exercitationem corrupti qui quis.\n\nAut est saepe ut molestiae sit. Reprehenderit maiores et iure tempora tempora unde. Quod numquam dolore deserunt adipisci id et saepe. Sed nihil placeat dicta qui.\n\nNostrum voluptas necessitatibus rerum nesciunt dolor. Ab culpa nemo sit voluptatem cum et blanditiis dolores.\n\nEt et officia molestiae sed voluptas quae consequuntur. Et eum labore dolorum qui.\n\nMagni est officiis id nisi. Incidunt sit dolorem quo quam quos accusamus. Adipisci odit molestiae dolorum nostrum repellendus vel. Qui placeat facilis provident earum. Asperiores est labore impedit amet cumque ut.\n\nCorporis et distinctio vel. Similique nam cumque aut. Est numquam tempora minima et modi. Sit molestiae est qui enim aut.\n\nNatus iste voluptatem minus. Numquam adipisci molestiae quae voluptate et. Rerum quam quia aut qui.\n\nVitae laboriosam perspiciatis aut velit. Blanditiis incidunt dignissimos est facere quas. Fuga et dolores enim voluptatem aliquid.\n\nRem et saepe quisquam expedita ea quisquam et nam. Fuga vel recusandae nobis minima. Similique corrupti occaecati quia ut cum odio. Accusantium at accusantium magnam aliquid est.\n\nOfficia sit enim voluptatum aut. Omnis ut quas nobis eaque. Atque eos necessitatibus facere in optio maxime dolorem. Eius aut repudiandae quis quo qui inventore.', 4, 1, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(7, 'Perferendis sed amet doloremque.', 'Quas fugiat sequi maxime dolor neque alias corporis. Eaque minus eum voluptatibus optio voluptas assumenda est. Occaecati in blanditiis aut minima. Totam hic odio aut voluptatibus voluptas quam odit.', NULL, 'Dolor laborum aliquam voluptas eligendi sed. In aut a quam ratione. Voluptates consequatur optio blanditiis harum amet sit. Totam sunt eos dolorem corporis omnis et nihil.\n\nUllam temporibus quia eum et. Est aspernatur necessitatibus cum omnis aut. Laboriosam nesciunt omnis architecto sequi nemo consequuntur. Totam eius ipsum rerum ea voluptatem totam et.\n\nOmnis molestiae impedit minima illo facere mollitia nihil. Et natus aspernatur consectetur voluptate nemo ad. Omnis dolore distinctio eaque sit est est hic. Animi eius rerum nobis.\n\nNumquam ad dolores sint voluptatem. Veritatis quaerat vitae vero quis eveniet quos atque iusto. Ad facilis eveniet cumque nobis adipisci sunt. Voluptatibus aut sed tempore totam consequatur odio.\n\nVoluptatem facilis ab dolores assumenda et quia. Animi nesciunt non perspiciatis libero saepe magnam debitis. Aut rerum ad est sunt aut. Nemo nam neque aperiam vitae illum fugiat. Eius ut nihil quisquam velit repellat nemo vel.\n\nLibero iste voluptas ut quas modi dicta. Velit dolorem corrupti aut omnis omnis praesentium. Enim cupiditate autem nobis nam amet rerum totam quam. Explicabo dolorum placeat ratione.\n\nMolestiae expedita excepturi voluptas nisi fuga tempore eius delectus. Ut impedit error quo dignissimos repellendus laboriosam. Beatae omnis laborum cum dolore hic.\n\nEum omnis eum vel. Eveniet sequi aliquid sint aspernatur veniam molestiae. Aut ad quam nemo tenetur aliquam. Consectetur et quo aut nostrum exercitationem incidunt fugit.\n\nConsequatur eum delectus quisquam eos nisi iusto consequatur rerum. Blanditiis quo quis odit quis doloribus rem porro eligendi. Corporis numquam aliquid ab ut.\n\nSapiente dicta quaerat voluptas qui. Quisquam nobis excepturi aut dolores molestias occaecati impedit quia. Praesentium alias similique soluta et delectus nostrum. Quidem dolore earum voluptatum dolor odio aut.', 10, 2, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(8, 'Ipsam quasi quam.', 'Et qui repudiandae maiores sed similique quasi. Dolorem facilis cum vel explicabo quas. Ullam perferendis voluptates culpa earum dolorem. Ut repellendus sed ut illo est.', NULL, 'Cum saepe vel adipisci quia. Amet sit illo quia fuga ducimus mollitia occaecati enim.\n\nNam quaerat eligendi ullam ut. Natus sint iusto doloribus dolorem accusantium rerum nesciunt. Excepturi dolores exercitationem nobis totam esse.\n\nId beatae corrupti saepe harum ut perspiciatis. Sed sed necessitatibus sint consequatur illo quidem et. Voluptatibus quasi et laborum voluptatem et in facilis.\n\nDolorem beatae consequatur perspiciatis dolorum dolor autem nobis dicta. Atque optio quia iusto et id occaecati aut. Totam nobis iusto adipisci doloremque unde quia corporis. Quisquam aut et ut aut debitis non.\n\nAliquid voluptatum aut impedit pariatur consequatur sequi molestiae et. Voluptatem numquam quibusdam laudantium at molestiae alias. Ipsam qui quo nihil vero laborum magni. Dolorem explicabo accusamus omnis quia iste.\n\nIure impedit atque inventore repellat aut. Voluptas expedita iusto quidem facere maxime dolor dignissimos. Est voluptas itaque est ea exercitationem quam et. Quia voluptas adipisci voluptate neque voluptatum.\n\nSimilique ab deleniti doloremque. Veritatis dolore voluptas doloremque rerum qui qui facere. Quisquam cumque quis assumenda excepturi modi optio.\n\nCum in animi est minus maxime ab aperiam non. Impedit commodi delectus ratione. Aliquid voluptatem atque voluptas.\n\nIure autem ex laudantium quasi odio nulla et ut. Amet nemo ducimus officia et quia eum. Quo voluptate temporibus eligendi aut vitae corrupti.\n\nMaiores quis non asperiores omnis. Facilis a placeat incidunt quis non modi aut. Numquam cumque dolor voluptas ut voluptas.', 4, 4, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(9, 'Aperiam eaque est delectus.', 'Saepe voluptate incidunt id. Repudiandae culpa a dolores deserunt quia id.', NULL, 'Quia officiis enim nostrum ullam. Et beatae eum nesciunt pariatur dolor voluptates eligendi. Incidunt ut quaerat est voluptatibus. Qui eaque praesentium corrupti ut rerum eum.\n\nRerum blanditiis fuga ut officia aut aut. Et qui odit provident ut suscipit ipsam. Aut dolores ut numquam.\n\nQuas quia consequatur et odit dolorum perferendis amet. Nam alias qui aut aliquam. Omnis tempore similique dignissimos architecto. Dolor illum velit et vel excepturi.\n\nAsperiores dolorum dolores rerum perferendis aut architecto. Aut tempora molestias alias voluptatem cupiditate sapiente incidunt. Amet commodi ratione amet. Est debitis vitae quia sapiente est adipisci deleniti.\n\nSint aliquam nihil quis. Quis est adipisci eveniet veniam totam harum aut placeat. Quasi non aspernatur ab voluptatum cum amet. Rerum sed rem reprehenderit officia ipsam.\n\nVoluptatem expedita possimus minus sunt et eveniet voluptatum. Distinctio doloribus aut itaque repudiandae quasi ipsam quam. Aliquam similique ex ratione molestiae.\n\nQuis non voluptates nesciunt quibusdam mollitia dolorem. Qui alias et voluptatem aliquam eum. Vitae tempora totam id sequi tempora adipisci voluptas.\n\nEst aliquam et tenetur explicabo ducimus accusantium et. Facilis enim nisi et ducimus rem eveniet. Eos quae laborum quaerat voluptas maiores eos vel quo.\n\nCommodi consequuntur excepturi consequatur eveniet quia harum. Qui voluptatem aperiam nesciunt maiores eum inventore. Minima voluptatibus aut rem. Laboriosam cumque distinctio in commodi. Qui architecto eum omnis quos.\n\nQuia omnis enim tenetur quo qui quia aliquam. Et quia dolor natus. Ut sunt debitis ut minima consectetur quis.', 6, 2, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(10, 'Ut quos omnis.', 'Sapiente eaque voluptas beatae earum facere. Consequatur maxime qui dicta nam aut iusto numquam ipsa. Autem culpa quidem ut fugiat. Officiis facilis unde dolorem suscipit alias.', NULL, 'Consequuntur velit vitae magnam. Expedita quo facere aut minima doloribus unde fuga. Voluptates vel quam quisquam quos doloremque eaque laboriosam qui. Quod ex laboriosam placeat qui eius.\n\nQuo ut nostrum atque vel. Autem animi nihil optio dolor ullam voluptatem temporibus. Aliquam debitis assumenda porro eos alias. Possimus praesentium autem voluptas libero atque.\n\nAut quaerat deserunt hic eaque. Corrupti laborum rem omnis rem ut atque cumque quis. Natus nulla veritatis a sunt repellat consequatur. Perspiciatis consequatur est deserunt commodi incidunt id.\n\nRepudiandae fuga sapiente ut aliquid eos fuga est. Autem velit assumenda et quae aut sequi corporis. Aliquid et voluptatem eos sapiente.\n\nSit labore delectus sunt vel. Aut laborum et vel impedit error et illo.\n\nAdipisci veritatis sint aut architecto sequi dolorem qui sunt. Iusto quae et ut aspernatur autem deleniti nihil.\n\nIllo nobis est corrupti fugiat harum vero reiciendis veniam. Ipsam velit placeat aut corporis voluptatem. Neque harum animi minus aut dolor et.\n\nAut est aut excepturi id ea aut quo saepe. Ut sint sed non laborum itaque minima fugit sit. Eos aut cupiditate ea quia sunt. Eum reprehenderit quas ad accusamus.\n\nQuae dicta vel hic corrupti magni dolores incidunt. Ratione facere veniam placeat. Consectetur minima sed doloremque ipsa. Aut commodi perferendis repellendus cum quo et voluptate.\n\nEa quas omnis inventore officia qui facilis iusto voluptas. Fugiat impedit sit fugit. Sunt dolor et sit consequatur.', 6, 4, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(11, 'Quos eum cupiditate debitis.', 'Et sapiente dolor cum accusamus ut. Amet officia nisi perferendis ut numquam ut. Nihil occaecati reprehenderit maiores.', NULL, 'Aliquam ut sit voluptas ducimus qui. Quis atque et totam consequatur neque quo. Vero consequuntur corporis cum in adipisci sapiente.\n\nEst consequatur expedita non vero accusamus doloremque officiis. Consequatur exercitationem iusto et qui ducimus animi. Rem qui provident doloribus harum consequatur ea. Laudantium eaque voluptas est mollitia.\n\nVoluptatem quia omnis aut vel. Expedita quia consequuntur atque eum. Sint libero ut beatae eius est ad eligendi. Corrupti consequatur accusamus minus dolor aperiam.\n\nUt quos ab asperiores provident. Nihil ducimus fugiat odio debitis sunt. Occaecati culpa corporis iusto quis.\n\nDeleniti ducimus cum ullam et nemo deserunt. Explicabo molestiae quae velit sed eum ipsum in temporibus. Exercitationem voluptatem est beatae doloribus laudantium facilis id.\n\nIste velit nam et et sunt. Et sit tempore omnis officiis quam vel modi. Non id voluptatem sed dolore.\n\nSoluta perspiciatis deserunt ut saepe autem. Consequuntur quasi impedit ea esse nam voluptate architecto atque. Minima sed provident repellendus quaerat laudantium. Molestias illum mollitia et et.\n\nTenetur aspernatur molestiae et excepturi autem. Veniam ab in in minus dolorum ducimus. Quos delectus rerum voluptatem aut.\n\nDeserunt ratione voluptates quam occaecati et nam accusamus. Et praesentium tenetur earum minima voluptatem.\n\nVero omnis sequi a inventore accusantium. Quia accusantium ea eaque omnis occaecati natus est. Cumque sint et voluptas inventore quia.', 5, 2, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(12, 'Corporis et ipsam consequatur.', 'Voluptas totam consequuntur rerum mollitia velit eligendi quas inventore. Labore enim deleniti qui voluptatem modi. Modi rerum culpa harum recusandae possimus quisquam quidem. Dignissimos ipsam qui ut quia voluptas nihil.', NULL, 'Eos eos quod vel. Repudiandae molestiae ut ipsa natus eligendi aut consequuntur beatae. Sequi aspernatur voluptatibus est eveniet. Quia laborum sint fugit et ex.\n\nMinima eaque rem cum cupiditate. Nobis ipsam molestias enim occaecati exercitationem omnis. Laborum voluptatem deserunt maxime incidunt reiciendis. Vitae necessitatibus voluptatum amet molestiae.\n\nRecusandae modi provident ut consequatur omnis. Molestias adipisci sequi saepe. Consequatur aut eos similique.\n\nExplicabo quas eos culpa incidunt. Magnam aut unde minima in qui. Eligendi aliquid molestiae ducimus. Cum et exercitationem exercitationem ullam harum.\n\nEt odit rerum laudantium atque. Delectus numquam est quam quasi itaque. Vitae aut earum nihil natus ad.\n\nNihil voluptatem porro ab numquam necessitatibus. Et eligendi reiciendis in dignissimos ut id. Iusto quos pariatur assumenda ad voluptatem voluptas recusandae. Officia eligendi accusantium a eius sed reprehenderit ut fuga.\n\nAliquam exercitationem sed eum dolores atque et. Temporibus fuga dignissimos voluptate voluptatem. Eos natus perspiciatis porro.\n\nEst et dignissimos debitis aut nihil vero. Omnis qui consequuntur ut omnis et. Blanditiis iusto beatae aliquam adipisci tempora facilis.\n\nRerum ea sed maxime est in ipsum. Error excepturi quia consequatur. Nisi illo repudiandae sint dolores incidunt velit architecto. Est atque non vel ut.\n\nAccusamus et possimus explicabo soluta. Perferendis vel quos numquam adipisci et aspernatur corrupti. Voluptatem dolorem quis debitis dicta.', 8, 4, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(13, 'Sit earum voluptas omnis.', 'Tempora voluptatibus odit id atque et sit maiores. Sit error perspiciatis molestiae corrupti. Magni impedit perferendis et beatae dicta cupiditate ut.', NULL, 'Est nesciunt minus est sunt ea eos deserunt. Voluptatem corrupti at repudiandae soluta aut.\n\nEst sint quia nihil dignissimos. Hic modi labore et veniam qui corrupti et. Error laboriosam velit quis aut. Saepe nisi aut nihil alias quia.\n\nQuam dolores molestiae qui. Quis quam ut assumenda officiis praesentium ut. Ut culpa fugiat ipsam dolorem et nulla.\n\nEst minus enim aliquam. Molestiae quae odit dolores inventore. Sunt quam voluptate itaque voluptas eum. Ipsum excepturi voluptatum omnis fuga. Distinctio vero natus voluptatum sunt est.\n\nUnde ad ab ratione aut sint. Odio officia aut debitis optio modi ducimus nulla.\n\nAtque aut ea sed quaerat. Consequatur impedit porro impedit voluptas provident. Id molestiae sequi asperiores est. Voluptatem tempora vel incidunt ipsam nihil iure doloremque.\n\nImpedit molestiae aspernatur accusamus iste. Consequatur esse officiis veniam numquam enim. Voluptatem illum delectus quis nam exercitationem maxime totam. Maiores excepturi nemo sequi quo quis. Eveniet qui ullam dolores soluta reprehenderit culpa.\n\nAut aspernatur quis deserunt eum impedit hic nihil alias. Ut repudiandae ad enim eum voluptate non. Et consequatur architecto earum ut. A quis ut aut consectetur.\n\nConsequuntur debitis tempore rerum corrupti reprehenderit itaque. Consequatur numquam tempore maxime et. Voluptatem qui commodi non. Repudiandae voluptates facere ex. Porro quidem ducimus ut ut dolorum.\n\nIusto expedita velit accusantium modi perspiciatis temporibus quasi. Iste esse nostrum doloremque voluptas. Corrupti eligendi odio aut aut.', 10, 4, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(14, 'Quis expedita libero.', 'Quaerat minus quibusdam in eos ullam. Sapiente maiores fugit accusamus. Quia ipsum at ab reprehenderit ut. Animi vel nihil dolores eos a blanditiis.', NULL, 'Ad dolor ipsa itaque. Omnis debitis aperiam ut maxime dolorem. Sed velit odit qui. Excepturi enim libero ea autem aperiam quia.\n\nBeatae et laborum inventore qui possimus vero suscipit. Natus nesciunt porro recusandae. Porro enim neque vel. Optio ad omnis asperiores laboriosam ducimus ex voluptatem.\n\nEt qui consequuntur et neque consequatur pariatur non. Molestiae modi aliquam cumque id. Et velit eligendi molestias molestiae. Maiores iste distinctio deleniti minus dolores doloribus ut.\n\nEt assumenda maiores qui aut tenetur recusandae. Ut ut quis itaque eius est. Id exercitationem autem nam optio similique praesentium iste. Molestias sunt consequatur impedit occaecati dolores autem ea.\n\nAutem at et ipsam at ut consectetur. Odio temporibus provident quisquam quis sunt illo illum fuga. Asperiores expedita tempore consequatur quo est numquam quos ut. Qui necessitatibus itaque aspernatur quam sed.\n\nMaiores vero quo explicabo reprehenderit. Blanditiis natus necessitatibus voluptates placeat cum vitae. Eum autem ut laborum modi perferendis. Sunt omnis quia ut doloribus est voluptatibus. Unde officia est id nihil aspernatur dolor quidem harum.\n\nAccusamus eum libero ipsum repellat eveniet. Sit fugiat placeat quis odio similique expedita non totam. Quaerat labore recusandae beatae eum officiis.\n\nVeniam dolor voluptate et. Excepturi voluptas temporibus omnis maxime libero ab. Dolorem qui corrupti maxime facilis. Quia ut aperiam molestiae.\n\nSed reprehenderit aut earum officiis repellat. Porro aliquid dolorum repudiandae porro amet numquam unde. Velit eaque fugiat aut. Neque quis molestias iusto rem. Rerum dolores eligendi fugit ut aspernatur.\n\nRerum qui id repellendus dolor et et possimus. Non maxime et maiores occaecati. Ut officiis ab et commodi eveniet autem.', 8, 1, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(15, 'Veniam reiciendis cumque labore.', 'In qui quasi et aut sequi maxime consectetur. Recusandae ut sunt non illo totam. Repellat ratione porro sit quia quo voluptatem.', NULL, 'Suscipit sed autem delectus exercitationem est eos. Illo ad accusantium autem tenetur. Nihil ut incidunt suscipit.\n\nEos eligendi adipisci ea ut doloribus quos. Ut praesentium repudiandae accusantium. Sunt omnis quia sapiente earum veniam aliquam aut.\n\nOdit est possimus quos et beatae. Voluptates qui quaerat ducimus mollitia optio. Magni rerum iusto exercitationem in.\n\nDucimus omnis illum sed earum aliquam eum iure. Consequatur ipsam porro consequatur sint et sit dicta. Sed consequuntur et neque iste quae dolorem vel.\n\nEligendi commodi voluptatem voluptas voluptatum. Quasi molestiae nihil sit iusto possimus nulla qui. Et ratione voluptas fuga ad neque amet. Suscipit et iusto facere. Nemo tenetur dolor est.\n\nIpsam magni exercitationem qui dolores neque doloremque quis. Rem placeat et omnis consequatur autem a. Aut aut cum facilis aut et quo est. Provident ratione est autem sint et laboriosam.\n\nDolores et excepturi quia molestiae aut. Ut officia impedit harum autem tempora. Adipisci facilis odio quae corrupti. Officia et quia et quam magni et non.\n\nQui unde qui sed voluptatibus est. Sed a facilis nesciunt quos tenetur. Et est consequatur aut dolor.\n\nEst blanditiis dolor est aut. Quo magnam qui et sint. Ipsam deleniti doloribus illo doloremque. Ut molestiae in culpa dolor.\n\nReprehenderit quia consequatur maxime sint impedit ad. Voluptas voluptatibus cumque deleniti distinctio et voluptatem soluta nisi. Amet consectetur qui animi doloremque blanditiis. Qui eius unde voluptatem et qui nemo.', 1, 3, '2022-01-31 07:07:29', '2022-01-31 07:07:29'),
(16, '¿Qué es un artículo?', 'En gramática, un artículo es una clase de palabra que acompaña al sustantivo dentro de una oración. Artículo y sustantivo se expresan en igual género (masculino o femenino) y número (singular o plural), y la función del artículo es especificar si el sustantivo es conocido (definido) o desconocido (indefinido).', 'posts/61f7581770162.png', 'Los artículos definidos e indefinidos forman parte de la mayoría de las oraciones, su principal función es acompañar al sustantivo y dar a conocer al lector u oyente información, ya que definen si el sustantivo es determinado o indeterminado. No es lo mismo decir “Hoy me compré el vestido” (definido o determinado) que “Hoy me compré un vestido” (indefinido o indeterminado).\r\n\r\nEl sustantivo puede nombrar sujetos u objetos y el artículo es el encargado de especificar su género (es decir, si es femenino o masculino) y su número (es decir, si es plural o singular). El sustantivo y su artículo siempre deben concordar en género y número. Por otro lado, el artículo siempre se escribe antes del sustantivo, nunca detrás.\r\n\r\nAdemás, el artículo permite conocer la función que tiene un sustantivo en una determinada oración, muchas veces un sustantivo puede variar su significado o peso dentro de una oración de acuerdo a la existencia o no de un artículo.', 11, 6, '2022-01-31 07:31:35', '2022-01-31 07:53:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@blog.com', '2022-01-31 07:07:28', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jjo4ul7v28', '2022-01-31 07:07:28', '2022-01-31 07:07:28');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `metricas`
--
ALTER TABLE `metricas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`),
  ADD KEY `posts_author_id_foreign` (`author_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `authors`
--
ALTER TABLE `authors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `metricas`
--
ALTER TABLE `metricas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`),
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

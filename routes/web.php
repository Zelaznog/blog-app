<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MetricaController;
use App\Http\Controllers\PostController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Landing', [
        'canLogin' => Route::has('login'),
        // 'canRegister' => Route::has('register'),
        // 'laravelVersion' => Application::VERSION,
        // 'phpVersion' => PHP_VERSION,
    ]);
})->name('inicial');

Route::get('/detail/{id}', function ()
{
    return Inertia::render('Detail') ;
})->name('detail');

Route::prefix('admin')->middleware('auth')->group(function ()
{
    // Ruta para el post
    Route::get('post', [PostController::class, 'index'])->name('post.admin');
    // Ruta para las categorias
    Route::get('category', [CategoryController::class, 'index'])->name('category.admin');
    // Rutas para los autores
    Route::get('author', [AuthorController::class, 'index'])->name('author.admin');
    // Ruta para la metricas
    Route::get('metrica', [MetricaController::class, 'index'])->name('metrica.admin');
});



require __DIR__.'/auth.php';

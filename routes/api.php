<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MetricaController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Rutas paras los posts
Route::prefix('post')->group(function ()
{
    Route::get('all', [PostController::class, 'allPost'])->name('post.all');
    Route::resource('adminPost', PostController::class);
});

// Rutas paras las categorias
Route::prefix('category')->group(function ()
{
    Route::get('all', [CategoryController::class, 'allCategory'])->name('category.all');
    Route::get('list', [CategoryController::class, 'allCategoryList'])->name('category.list');
    Route::resource('adminCategory', CategoryController::class);
});

// Rutas paras los autores
Route::prefix('authors')->group(function ()
{
    Route::get('all', [AuthorController::class, 'allAuthors'])->name('author.all');
    Route::get('list', [AuthorController::class, 'allAuthorsList'])->name('author.list');
    Route::resource('adminAuthor', AuthorController::class);
});
// Ruta para las metricas 
Route::prefix('metricas')->group(function ()
{
    Route::get('publicaciones', [MetricaController::class, 'getPublicaciones'])->name('publicaciones.metrica');
    Route::get('dispositivos', [MetricaController::class, 'getDivices'])->name('dispositivos.metrica');
    Route::get('plataforma', [MetricaController::class, 'getPlataforma'])->name('plataforma.metrica');
    Route::get('navegador', [MetricaController::class, 'getNavegador'])->name('navegador.metrica');
});

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'title' => $this->faker->sentence(3), 
            'resume' => $this->faker->paragraph(), 
            'body' => $this->faker->paragraphs(10, true),
            'category_id' => $this->faker->numberBetween(1, 10), 
            'author_id' => $this->faker->numberBetween(1, 5), 
        ];
    }
}

<?php

use Facade\Ignition\Tabs\Tab;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetricasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metricas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id')->unsigned();
            $table->ipAddress('ipuser')->comment('direccion ip del usuario');
            $table->string('plataform', 40)->comment('plataforma donde visualizo el post');
            $table->string('browser', 40)->comment('navedador donde se visualizo el post');
            $table->boolean('is_divice')->default(0)->comment('0 - desktop, 1 - mobile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metricas');
    }
}
